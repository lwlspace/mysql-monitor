//引入路由对象
import {createRouter,createWebHashHistory} from'vue-router'
//懒加载
const Digest = ()=>import('../pages/events/Digest.vue')
const Index = ()=>import('../pages/events/Index.vue')
const TableIndexes = ()=>import('../pages/events/TableIndexes.vue')
const Connections = ()=>import('../pages/events/Connections.vue')
const Profile = ()=>import('../pages/events/Profile.vue')
const FileSummary = ()=>import('../pages/events/FileSummary.vue')

//创建路由对象的url映射表
const routes = [
    //配置路由对象
    {path: '/',redirect:'/events/index'},
    {path: '/events/index',component:Index},
    {path: '/events/digest',component:Digest},
    {path: '/events/tableIndexes',component:TableIndexes},
    {path: '/events/connections',component:Connections},
    {path: '/events/profile',component:Profile},
    {path: '/events/fileSummary',component:FileSummary}
];
//创建路由对象
const router = createRouter({
    //路由模式 hash/history
    history:createWebHashHistory(),
    //内部所有路由
    routes
})

//导出路由对象
export default router