import axios from "axios";

//axios的全局配置
axios.defaults.baseURL = "http://localhost:8088/monitor";
//timeout的单位为毫秒ms
axios.defaults.timeout = 5000;
//该格式为默认格式，如果传输文件这里需要配置
axios.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded';

//axios配置拦截
//请求拦截
//作用：1.跨域配置token，2.请求出去了，显示过渡动画，注意需要返回config
axios.interceptors.response.use((response)=>{
    //响应数据是什么这里打印的就是什么
  //  console.log(response);
    return response;
});
//封装的请求方法
export default function ajax(url='',params={},type='GET') {
    //定义一个变量
    let promise;
    //返回一个promise链式调用
    return new Promise((resolve, reject)=>{
        //1.判断请求的类型，统一转为大写
        if(type.toUpperCase() === 'GET'){
            //get请求
            promise = axios({
                url:url,
                params:params
            })
        }else if(type.toUpperCase() === 'POST'){
            //post请求
            promise = axios({
                method:"post",
                url:url,
                data:params,
            })
        }
        //处理返回
        promise.then((res)=>{
            //处理返回,链式调用
            resolve(res);
        }).catch((error)=>{
            reject(error);
        })
    })
}