
import { createApp } from 'vue'
import App from './App.vue'
//导入Element组件库
import ElementPlus from 'element-plus'
import 'element-plus/dist/index.css'
//导入全局路由对象
import router from "./router/index.js";
//导入vuex对象
import store from './store/store.js'



const app = createApp(App);

//异常捕捉~
app.config.errorHandler = function(err, vm, info) {
    console.log(`Error: ${err.toString()}\nInfo: ${info}`);
}

app.use(router);
app.use(store);
app.use(ElementPlus);
app.mount('#app')

