import {createStore} from "vuex";

//创建一个store对象
const store = createStore({
    state:{
        //sql语句执行次数事件(Map集合)
        eventsDigest:null,
        //选择的查询次数事件(value)
        selectEventsDigest:null,
        //table表索引（Map集合)
        tableIndex:null,
        //选中的table表索引值(value)
        selectTableIndex:null,
        //单选框,数据库标签集合
        DNameArr:null,
        //选中的数据库
        selectDBName:null,

        //sql语句各属性是否能排序的标志(为空时不可排序)
        EventsDigestSort:true,
        //索引各属性是否能排序的标志(为空时不可排序)
        TableIndexSort:true,
        //切换Head的多选框 数据库/用户连接
        toggleHead:null,

        //用户连接数组
        users:null,
        //选择的用户
        selectedUser:null,
        //用户连接线程集合
        usersThreadMap:null,
        //选中的用户连接
        usersThread:null,

        //事件名称集合
        fileNameArr:null,
        //存储事件详细
        fileSummary:null,
        //选择的事件
        selectedEvent:null,
        //选择的事件概况
        eventSummary:null,
    },
    getters:{

    },
    actions:{
        setEventsDigest({commit},eventsDigest){
            commit('setEventsDigest',eventsDigest)
        },
        selectEventsDigest({commit,state},currentVal){
            let flag = true;
            for(let db in store.state.eventsDigest){
                if(db===currentVal){
                    state.EventsDigestSort = true;
                    commit('selectEventsDigest',store.state.eventsDigest[db]);
                    flag = false;
                }
            }
            if(flag){
                state.EventsDigestSort = false;
                commit('selectEventsDigest',null);
            }

        },
        setNameArr({commit},arr) {
            commit('setNameArr', arr);
        },
        setTableIndex({commit},arr){
            commit('setTableIndex',arr);
        },
        selectTableIndex({commit,state},currentValue){
            let flag = true;
            for(let db in store.state.tableIndex){
                if(db === currentValue){
                    state.TableIndexSort = true;
                    flag = false;
                    commit('selectTableIndex',store.state.tableIndex[db]);
                }
            }
            if(flag){
                //为空时
                state.TableIndexSort = false;
                commit('selectTableIndex',null);
            }
        },
        setUsersConnection({commit},users){
            commit("setUsersConnection",users);
        },
        setUsersThreadMap({commit},usersMap){
            commit('setUsersThreadMap',usersMap);
        },
        selectUserConnection({commit,state},currentUser){
            let flag = true
            if(currentUser!=='usersConnectionDetail'){
                //usersConnectionDetail是存储用户连接详细的集合
                for(let userKey in state.usersThreadMap){
                    if(userKey === currentUser){
                        flag = false;
                        commit('selectUserConnection',state.usersThreadMap[userKey])
                    }
                }
            }
            if(flag){
                commit('selectUserConnection',null);
            }
        },
        setFileNameArr({commit},fileNameArr){
            commit('setFileNameArr',fileNameArr);
        },
        selectEventFile({commit,state},eventName){
            // 对应value无"空"情况，所以直接赋值，无需判断
            commit('selectEventFile',state.fileSummary[eventName]);
        }

    },
    mutations:{
        setToggleHead(state,flag){
            state.toggleHead = flag;
        },
        setEventsDigest(state,eventsDigest){
            state.eventsDigest = eventsDigest;
        },
        selectEventsDigest(state,eventsDigest){
            state.selectEventsDigest = eventsDigest;
        },
        setNameArr(state,arr){
            state.DNameArr = arr;
        },
        setTableIndex(state,tableIndex){
            state.tableIndex = tableIndex;
        },
        selectTableIndex(state,tableIndex){
            state.selectTableIndex = tableIndex;
        },
        setUsersConnection(state,users){
            state.users = users;
        },
        setUsersThreadMap(state,usersMap){
            state.usersThreadMap = usersMap;
        },
        selectUserConnection(state,currentUserConn){
            state.usersThread = currentUserConn;
        },
        setFileNameArr(state,fileNameArr){
            state.fileNameArr = fileNameArr;
        },
        setFileSummary(state,fileSummary){
            state.fileSummary = fileSummary;
        },
        selectEventFile(state,eventSummary){
            state.eventSummary = eventSummary;
        }
    },
});

//导出store对象
export default store;