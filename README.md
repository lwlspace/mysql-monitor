# mysqlMonitor
建议安装使用版:[一键启动](https://gitee.com/lwlspace/monitor_mysql)
#### 介绍
MySQL监控系统（前端基于Vue3.0，使用了Element-PlusUI框架/后端基于SpringBoot）架构如下图:
![输入图片说明](1-mysql_monitor.drawio.png)

#### 项目展示
![输入图片说明](https://images.gitee.com/uploads/images/2021/1123/112950_c20aa910_9450095.png "060ccb260fa8b89ff1635a0ef1457d7.png")
![输入图片说明](https://images.gitee.com/uploads/images/2021/1123/112958_f6e02390_9450095.png "23498058268252eceeb66b6ce14de0e.png")
![输入图片说明](https://images.gitee.com/uploads/images/2021/1123/113006_a2459352_9450095.png "baf6626893edec06665a95cceb5eadc.png")
![输入图片说明](https://images.gitee.com/uploads/images/2021/1123/112938_d893bf6c_9450095.png "5f38d2adcec01e957b67a5c635542b1.png")
![输入图片说明](https://images.gitee.com/uploads/images/2021/1123/113016_5ef5c3e7_9450095.png "ceebf7b0a345b0bd03a68cb6a1fb31a.png")

#### 安装教程

1.  前端:Node的版本为v14.17.3,vue-Cli版本为@vue/cli 4.5.13(能支持vue3即可），推荐使用IDEA,导入项目后在当前目录下npm install下载依赖，然后使用npm run dev便可启动前端项目
2.  后端:推荐使用IDEA,JDK推荐1.8,导入之后需加载依赖之后，检查连接url和username以及password(若为其他版本的MySQL还需在pom下修改依赖以及更改url)便可run了
3.  MySQL数据库:依赖于本地MySQL自带的Perfomance-Schema库（当前项目使用的Mysql版本为5.7),使用之前请查看MySQL中是否有该库

#### 使用说明

1.  若出现端口占用需修改端口号，前端可在vite.config.js下修改(修改后请在后端的config目录下的CrossConfiguration中配置跨域访问的放行)，后端在application.yml下修改
2.  不支持IE

#### 设计思路
该项目是由MySQL5版本新增后的数据库模块Performance_schema性能板块监控为基础开发的
我整理的一些思路碎片放在这：<br>
MySQL5.7关于Performance_schema的简单介绍和注意事项:[笔记](https://blog.csdn.net/killbibi/article/details/121489697)<br>
[MySQL5.7关于Performance_schema官方文档介绍-中文](https://www.docs4dev.com/docs/zh/mysql/5.7/reference/performance-schema-table-index.html)<br>
由于该中文版本对一些概念翻译的不是很到位，这里放上官方介绍
[MySQL5.7关于Performance_schema官方文档介绍-英文原版](https://dev.mysql.com/doc/refman/5.7/en/performance-schema-threads-table.html)


#### 后言
该项目前后端构思开发测试由我完成，若存在Bug或不足，可告知我，谢谢~
微信号:L2574308236